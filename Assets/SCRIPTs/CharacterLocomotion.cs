﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class CharacterLocomotion : MonoBehaviour
{

    vThirdPersonController tpController;
    vShooterMeleeInput tpInput;

    public AK.Wwise.Event wwiseEvent;
    

    

    public LayerMask lm;
    public string surface;


    public GameObject footL;
    public GameObject footR;


    public void Start()
    {
        tpController = GetComponent<vThirdPersonController>();
        tpInput = GetComponent<vShooterMeleeInput>();
    }

    
    
    
    public void PlayFootstepsL()
    {
        SurfaceCheckL();

        if(tpInput.cc.inputMagnitude > 0.1)
        {
            if (tpController.isSprinting)
            {

                AkSoundEngine.SetSwitch("Locomotion", "Running", footL);
                AkSoundEngine.SetSwitch("Surf", surface, footL);
                wwiseEvent.Post(footL);
                

                Debug.Log("Running");

            }

            else
            {

                AkSoundEngine.SetSwitch("Locomotion", "Walking", footL);
                AkSoundEngine.SetSwitch("Surf", surface, footL);
                wwiseEvent.Post(footL);
                
                Debug.Log("Walking");


            }



        }






    }

    public void PlayFootstepsR()
    {
        SurfaceCheckR();

        if (tpInput.cc.inputMagnitude > 0.1)
        {
            if (tpController.isSprinting)
            {

                AkSoundEngine.SetSwitch("Locomotion", "Running", footR);
                AkSoundEngine.SetSwitch("Surf", surface, footR);
                wwiseEvent.Post(footR);
                

                Debug.Log("Running");

            }

            else
            {

                AkSoundEngine.SetSwitch("Locomotion", "Walking", footR);
                AkSoundEngine.SetSwitch("Surf", surface, footR);
                wwiseEvent.Post(footR);
                
                Debug.Log("Walking");


            }



        }






    }

    void SurfaceCheckL()
    {
        if(Physics.Raycast(footL.transform.position, Vector3.down, out RaycastHit rh, lm))
        {
            switch (rh.collider.tag)
            {

                case "Concrete":
                    surface = "Concrete";
                    break;

                case "Tile":
                    surface = "Tile";
                    break;

                default:
                    surface = "Concrete";
                    break;




            }


            Debug.Log(surface + " L");

        }



    }

    void SurfaceCheckR()
    {
        if (Physics.Raycast(footL.transform.position, Vector3.down, out RaycastHit rh, lm))
        {
            switch (rh.collider.tag)
            {

                case "Concrete":
                    surface = "Concrete";
                    break;

                case "Tile":
                    surface = "Tile";
                    break;

                default:
                    surface = "Concrete";
                    break;




            }




        }

        Debug.Log(surface + " R");

    }
}
