﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vShooter;
using Invector.vItemManager;

public class m16oneshot : MonoBehaviour
{
    public AK.Wwise.Event wwiseEvent;
    public string Switch;

    AmmoCOunt Ammo;
    public int AmmoInWeapon;

    private void Start()
    {
        Ammo = GetComponent<AmmoCOunt>();
    }

    private void Update()
    {
        Ammo.AmmoCount();
        AmmoInWeapon = Ammo.currentAmmo;
    }


    public void Shot()
    {
       
        checkTrigger();
        
        
        AkSoundEngine.SetRTPCValue("GunAmmoCount", AmmoInWeapon);


        wwiseEvent.Post(gameObject);



    }

    void checkTrigger()
    {
        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Weapon")
            {
                AkSoundEngine.SetSwitch("M16", "Room", gameObject);
                

            }
            Switch = "Room";

        }

        void OnTriggerExit(Collider other)
        {
            if (other.tag == "Weapon")
            {
                AkSoundEngine.SetSwitch("M16", "Street", gameObject);
               

            }
            Switch = "Street";
        }

        Debug.Log(Switch);
    }

    
}
