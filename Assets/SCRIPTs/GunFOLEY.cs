﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFOLEY : MonoBehaviour
{
    public AK.Wwise.Event Reload;
    public AK.Wwise.Event HandleIn;
    public AK.Wwise.Event HandleOut;

    public void ReloadGUN()
    {
        Reload.Post(gameObject);
    }

    public void HandleInGUN()
    {
        HandleIn.Post(gameObject);
    }

    public void HandleOUTGUN()
    {
        HandleOut.Post(gameObject);
    }
}
