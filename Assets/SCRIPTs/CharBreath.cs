﻿using Invector.vCharacterController;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharBreath : MonoBehaviour
{
    vThirdPersonController tpController;
    vMeleeCombatInput tpInput;

    public AK.Wwise.Event wwiseEvent;
    public GameObject player;
   
    private void Start()
    {
        tpController = GetComponent<vThirdPersonController>();
        tpInput = GetComponent<vMeleeCombatInput>();
        playBreath();
    }

    private void Update()
    {
        breath();
    }

    void breath()
    {


        float stamina = tpController.currentStamina;
        AkSoundEngine.SetRTPCValue("Stamina", stamina);



    }

    void playBreath()
    {
        wwiseEvent.Post(player);



    }
}
