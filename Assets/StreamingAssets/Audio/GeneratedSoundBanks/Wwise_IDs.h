/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AMB_ROOM = 3202361783U;
        static const AkUniqueID AMB_ROOM_SPOT1 = 1899016623U;
        static const AkUniqueID AMB_ROOM_SPOT2 = 1899016620U;
        static const AkUniqueID AMB_STREET = 1592801615U;
        static const AkUniqueID BOMB = 1727945837U;
        static const AkUniqueID LOCOMOTION = 556887514U;
        static const AkUniqueID METAL = 2473969246U;
        static const AkUniqueID METAL_2 = 3348275853U;
        static const AkUniqueID TENT = 3207335970U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace AMB
        {
            static const AkUniqueID GROUP = 1117531639U;

            namespace STATE
            {
                static const AkUniqueID INSIDE = 3553349781U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OUTSIDE = 438105790U;
            } // namespace STATE
        } // namespace AMB

    } // namespace STATES

    namespace SWITCHES
    {
        namespace LOCOMOTION
        {
            static const AkUniqueID GROUP = 556887514U;

            namespace SWITCH
            {
                static const AkUniqueID RUNNING = 3863236874U;
                static const AkUniqueID WALKING = 340271938U;
            } // namespace SWITCH
        } // namespace LOCOMOTION

        namespace SURF
        {
            static const AkUniqueID GROUP = 336035877U;

            namespace SWITCH
            {
                static const AkUniqueID CONCRETE = 841620460U;
                static const AkUniqueID TILE = 2637588553U;
            } // namespace SWITCH
        } // namespace SURF

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
